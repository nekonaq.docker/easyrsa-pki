ARG BASE_IMAGE
FROM $BASE_IMAGE

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

RUN set -x \
&& export DEBIAN_FRONTEND=noninteractive \
&& apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y openssl \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
;

COPY easy-rsa/easyrsa3 /usr/share/easy-rsa
COPY docker-entrypoint /

ENV PATH /usr/share/easy-rsa:$PATH
ENV EASYRSA_PKI /easyrsa/pki

VOLUME /easyrsa
WORKDIR /easyrsa

ENTRYPOINT ["/docker-entrypoint"]
